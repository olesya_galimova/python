import unittest
import lab2


class TestStringMethods(unittest.TestCase):
    def test_is_neighbours_true(self):
        matrix = ((0, 0, 0, 0, 0),
                  (0, 1, 0, 0, 0),
                  (0, 1, 1, 1, 0),
                  (0, 0, 0, 0, 0))
        self.assertEqual(lab2.is_neighbours(lab2.Point(2, 2), matrix), True)

    def test_is_neighbours_false(self):
        matrix = ((0, 0, 0, 0, 0),
                  (0, 1, 0, 0, 0),
                  (0, 1, 1, 1, 0),
                  (0, 0, 0, 0, 0))
        self.assertEqual(lab2.is_neighbours(lab2.Point(2, 1), matrix), False)

    def test_neighbours(self):
        matrix = ((0, 0, 0, 0, 0),
                  (0, 1, 0, 0, 0),
                  (0, 1, 1, 1, 0),
                  (0, 0, 0, 0, 0))
        self.assertEqual(lab2.neighbours(lab2.Point(1, 2), matrix),
                         [lab2.Point(2, 2), lab2.Point(1, 1)])

    def test_neighbours_false(self):
        matrix = ((0, 0, 0, 0, 0),
                  (0, 1, 0, 0, 0),
                  (0, 1, 1, 1, 0),
                  (0, 0, 0, 0, 0))
        self.assertEqual(lab2.neighbours(lab2.Point(3, 0), matrix), [])

    def test_find_island(self):
        matrix = ((0, 0, 0, 0, 0),
                  (0, 1, 0, 0, 0),
                  (0, 1, 0, 1, 0),
                  (0, 0, 0, 0, 0))
        vec = [lab2.Point(1, 1), lab2.Point(1, 2)]
        self.assertEqual(lab2.find_island(matrix, lab2.Point(1, 1)), vec)

    def test_find_count_islands(self):
        matrix = ((0, 0, 0, 0, 0),
                  (0, 1, 0, 0, 0),
                  (0, 1, 0, 1, 0),
                  (0, 0, 0, 0, 0))
        self.assertEqual(lab2.find_count_islands(matrix), 2)

    def test_find_count_islands_no_islands(self):
        matrix = ((0, 0, 0, 0, 0),
                  (0, 0, 0, 0, 0),
                  (0, 0, 0, 0, 0),
                  (0, 0, 0, 0, 0))
        self.assertEqual(lab2.find_count_islands(matrix), 0)


if __name__ == '__main__':
    unittest.main()
