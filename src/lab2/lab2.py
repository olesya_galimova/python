from dataclasses import dataclass


@dataclass(frozen=True)
class Point:
    x: int
    y: int


def is_neighbours(point, matrix):
    x, y = point.x, point.y
    lenght = len(matrix[0])
    hight = len(matrix)
    if 0 <= x < lenght and 0 <= y < hight and matrix[y][x]:
        return True
    return False


def neighbours(point, matrix):
    x, y = point.x, point.y
    possible_neighbours_list = [Point(x, y + 1),
                                Point(x + 1, y),
                                Point(x, y - 1),
                                Point(x - 1, y)]
    neighbours_list = []
    for p in possible_neighbours_list:
        if is_neighbours(p, matrix):
            neighbours_list.append(p)
    return neighbours_list


def find_island(matrix, start_pos):
    visited = []
    points = [start_pos]
    while points:
        point = points.pop()
        visited.append(point)
        for neighbour in neighbours(point, matrix):
            if neighbour in visited:
                continue
            points.append(neighbour)
    return visited


def find_count_islands(matrix):
    visited = []
    lenght = len(matrix[0])
    hight = len(matrix)
    count = 0
    for i in range(hight):
        for j in range(lenght):
            point = Point(j, i)
            if matrix[i][j] and point not in visited:
                visited.extend(find_island(matrix, point))
                count += 1
    return count


def read_matrix():
    n = int(input("Enter count of strings: "))
    matrix = [[int(j) for j in input().split()] for i in range(n)]
    return matrix


# matrix = read_matrix()
# print(find_count_islands(matrix))
