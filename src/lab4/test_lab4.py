import unittest
import lab4
from lab4 import Vertex


class TestStringMethods(unittest.TestCase):
    def test_transp(self):
        graph = {1: Vertex(0, 0, [2, 3]),
                 2: Vertex(0, 0, [4, 5]),
                 3: Vertex(0, 0, []),
                 4: Vertex(0, 0, [6]),
                 5: Vertex(0, 0, [3]),
                 6: Vertex(0, 0, [1])}
        res = {1: Vertex(0, 0, [6]),
               2: Vertex(0, 0, [1]),
               3: Vertex(0, 0, [1, 5]),
               4: Vertex(0, 0, [2]),
               5: Vertex(0, 0, [2]),
               6: Vertex(0, 0, [4])}
        self.assertEqual(lab4.transp(graph), res)

    def test_transp2(self):
        graph = {1: Vertex(0, 0, [2]),
                 2: Vertex(0, 0, [3, 4, 7]),
                 3: Vertex(0, 0, []),
                 4: Vertex(0, 0, [2, 5, 6]),
                 5: Vertex(0, 0, [8, 11]),
                 6: Vertex(0, 0, [7, 8]),
                 7: Vertex(0, 0, [6]),
                 8: Vertex(0, 0, [9]),
                 9: Vertex(0, 0, [10]),
                 10: Vertex(0, 0, [11]),
                 11: Vertex(0, 0, [12]),
                 12: Vertex(0, 0, [5])}
        res = {1: Vertex(0, 0, []),
               2: Vertex(0, 0, [1, 4]),
               3: Vertex(0, 0, [2]),
               4: Vertex(0, 0, [2]),
               5: Vertex(0, 0, [4, 12]),
               6: Vertex(0, 0, [4, 7]),
               7: Vertex(0, 0, [2, 6]),
               8: Vertex(0, 0, [5, 6]),
               9: Vertex(0, 0, [8]),
               10: Vertex(0, 0, [9]),
               11: Vertex(0, 0, [5, 10]),
               12: Vertex(0, 0, [11])}
        self.assertEqual(lab4.transp(graph), res)

    def test_sccs(self):
        graph = {1: Vertex(0, 0, [2, 3]),
                 2: Vertex(0, 0, [4, 5]),
                 3: Vertex(0, 0, []),
                 4: Vertex(0, 0, [6]),
                 5: Vertex(0, 0, [3]),
                 6: Vertex(0, 0, [1])}
        res = [{1, 2, 4, 6}, {5}, {3}]
        self.assertEqual(lab4.sccs(graph), res)

    def test_sccs2(self):
        graph = {1: Vertex(0, 0, [2]),
                 2: Vertex(0, 0, [3, 4, 7]),
                 3: Vertex(0, 0, []),
                 4: Vertex(0, 0, [2, 5, 6]),
                 5: Vertex(0, 0, [8, 11]),
                 6: Vertex(0, 0, [7, 8]),
                 7: Vertex(0, 0, [6]),
                 8: Vertex(0, 0, [9]),
                 9: Vertex(0, 0, [10]),
                 10: Vertex(0, 0, [11]),
                 11: Vertex(0, 0, [12]),
                 12: Vertex(0, 0, [5])}
        res = [{1}, {2, 4}, {6, 7}, {5, 8, 9, 10, 11, 12}, {3}]
        self.assertEqual(lab4.sccs(graph), res)


if __name__ == '__main__':
    unittest.main()
