from dataclasses import dataclass


@dataclass()
class Vertex:
    in_time: int
    out_time: int
    next: list


def read_graph():
    graph = {}
    s = input()
    while s:
        ind = int(s)
        s = input()
        graph[ind] = Vertex(0, 0, [int(i) for i in s.split()])
        s = input()
    return graph


def dfs(graph, start, clock, visited):
    clock += 1
    graph[start].in_time = clock
    visited.add(start)
    for neighbour in graph[start].next:
        if neighbour in visited:
            continue
        clock = dfs(graph, neighbour, clock, visited)
    clock += 1
    graph[start].out_time = clock
    return clock


def topological_sort(graph):
    visited = set()
    clock = 0
    for vert in graph:
        if vert in visited:
            continue
        clock = dfs(graph, vert, clock, visited)
    sort = sorted(graph.keys(),
                  key=lambda item: graph[item].out_time, reverse=True)
    return sort


def transp(graph):
    transp_gr = {}
    for vert in graph:
        transp_gr[vert] = Vertex(0, 0, [])
    for vert in graph:
        for neighbour in graph[vert].next:
            transp_gr[neighbour].next.append(vert)
    return transp_gr


def sccs(graph):
    sort_gr = topological_sort(graph)
    transp_gr = transp(graph)
    visited = set()
    strong_conv = []
    for vert in sort_gr:
        if vert in visited:
            continue
        buf = visited.copy()
        dfs(transp_gr, vert, 0, visited)
        buf = visited.difference(buf)
        strong_conv.append(buf)
    return strong_conv


# gr = read_graph()
# print(sccs(gr))
