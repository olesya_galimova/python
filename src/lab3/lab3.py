from dataclasses import dataclass


@dataclass()
class Vertex:
    In: int
    Out: int
    Next: list


def read_graph():
    graph = {}
    str = input()
    while str:
        ind = int(str)
        str = input()
        graph[ind] = Vertex(0, 0, [int(i) for i in str.split()])
        str = input()
    return graph


def dfs(graph, start, clock, visited):
    clock += 1
    graph[start].In = clock
    visited.append(start)
    for neighbour in graph[start].Next:
        if neighbour in visited:
            continue
        clock = dfs(graph, neighbour, clock, visited)
    clock += 1
    graph[start].Out = clock
    return clock


def topological_sort(graph):
    visited = []
    clock = 0
    for vert in graph.keys():
        if vert in visited:
            continue
        clock = dfs(graph, vert, clock, visited)
    sort = sorted(graph.keys(), key=lambda item: graph[item].Out, reverse=True)
    return sort


# graph = read_graph()
# sort = topological_sort(graph)
# print(sort)
