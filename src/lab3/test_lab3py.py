import unittest
import lab3


class TestStringMethods(unittest.TestCase):
    def test_dfs(self):
        graph = {1: lab3.Vertex(0, 0, [2, 3]),
                 2: lab3.Vertex(0, 0, []),
                 3: lab3.Vertex(0, 0, []),
                 4: lab3.Vertex(0, 0, [5, 7]),
                 5: lab3.Vertex(0, 0, [1]),
                 6: lab3.Vertex(0, 0, [8, 10]),
                 7: lab3.Vertex(0, 0, []),
                 8: lab3.Vertex(0, 0, []),
                 9: lab3.Vertex(0, 0, []),
                 10: lab3.Vertex(0, 0, [9])}
        start = 10
        visited = []
        clock = 0
        out = {1: lab3.Vertex(0, 0, [2, 3]),
               2: lab3.Vertex(0, 0, []),
               3: lab3.Vertex(0, 0, []),
               4: lab3.Vertex(0, 0, [5, 7]),
               5: lab3.Vertex(0, 0, [1]),
               6: lab3.Vertex(0, 0, [8, 10]),
               7: lab3.Vertex(0, 0, []),
               8: lab3.Vertex(0, 0, []),
               9: lab3.Vertex(2, 3, []),
               10: lab3.Vertex(1, 4, [9])}
        self.assertEqual(lab3.dfs(graph, start, clock, visited), 4)
        self.assertEqual(graph, out)
        self.assertEqual(visited, [10, 9])

    def test_dfs2(self):
        graph = {1: lab3.Vertex(1, 6, [2, 3]),
                 2: lab3.Vertex(2, 3, []),
                 3: lab3.Vertex(4, 5, []),
                 4: lab3.Vertex(0, 0, [5, 7]),
                 5: lab3.Vertex(0, 0, [1]),
                 6: lab3.Vertex(0, 0, [8, 10]),
                 7: lab3.Vertex(0, 0, []),
                 8: lab3.Vertex(0, 0, []),
                 9: lab3.Vertex(0, 0, []),
                 10: lab3.Vertex(0, 0, [9])}
        start = 4
        visited = [1, 2, 3]
        clock = 6
        out = {1: lab3.Vertex(1, 6, [2, 3]),
               2: lab3.Vertex(2, 3, []),
               3: lab3.Vertex(4, 5, []),
               4: lab3.Vertex(7, 12, [5, 7]),
               5: lab3.Vertex(8, 9, [1]),
               6: lab3.Vertex(0, 0, [8, 10]),
               7: lab3.Vertex(10, 11, []),
               8: lab3.Vertex(0, 0, []),
               9: lab3.Vertex(0, 0, []),
               10: lab3.Vertex(0, 0, [9])}
        self.assertEqual(lab3.dfs(graph, start, clock, visited), 12)
        self.assertEqual(graph, out)
        self.assertEqual(visited, [1, 2, 3, 4, 5, 7])

    def test_toptlogical_sort(self):
        graph = {1: lab3.Vertex(0, 0, [2, 3]),
                 2: lab3.Vertex(0, 0, []),
                 3: lab3.Vertex(0, 0, []),
                 4: lab3.Vertex(0, 0, [5, 7]),
                 5: lab3.Vertex(0, 0, [1]),
                 6: lab3.Vertex(0, 0, [8, 10]),
                 7: lab3.Vertex(0, 0, []),
                 8: lab3.Vertex(0, 0, []),
                 9: lab3.Vertex(0, 0, []),
                 10: lab3.Vertex(0, 0, [9])}
        self.assertEqual(lab3.topological_sort(graph),
                         [6, 10, 9, 8, 4, 7, 5, 1, 3, 2])

    def test_toptlogical_sort2(self):
        graph = {1: lab3.Vertex(0, 0, [2, 3, 4]),
                 2: lab3.Vertex(0, 0, [5, 6, 8]),
                 3: lab3.Vertex(0, 0, [7]),
                 4: lab3.Vertex(0, 0, []),
                 5: lab3.Vertex(0, 0, []),
                 6: lab3.Vertex(0, 0, []),
                 7: lab3.Vertex(0, 0, [8]),
                 8: lab3.Vertex(0, 0, [5])}
        self.assertEqual(lab3.topological_sort(graph),
                         [1, 4, 3, 7, 2, 8, 6, 5])


if __name__ == '__main__':
    unittest.main()
