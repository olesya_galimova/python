import unittest
import lab5
from lab5 import Edge


class TestStringMethods(unittest.TestCase):
    def test_make_set(self):
        parent = {'A': 'A', 'B': 'B'}
        rank = {'A': 0, 'B': 0}
        new_el = 'C'
        new_parent = {'A': 'A', 'B': 'B', 'C': 'C'}
        new_rank = {'A': 0, 'B': 0, 'C': 0}
        lab5.make_set(parent, rank, new_el)
        self.assertEqual(parent, new_parent)
        self.assertEqual(rank, new_rank)

    def test_make_set_empty(self):
        parent = {}
        rank = {}
        new_el = 'A'
        new_parent = {'A': 'A'}
        new_rank = {'A': 0}
        lab5.make_set(parent, rank, new_el)
        self.assertEqual(parent, new_parent)
        self.assertEqual(rank, new_rank)

    def test_find_root(self):
        parent = {'A': 'A'}
        find_el = 'A'
        self.assertEqual(lab5.find(parent, find_el), 'A')

    def test_find(self):
        parent = {'A': 'A', 'B': 'A', 'C': 'B'}
        find_el = 'C'
        self.assertEqual(lab5.find(parent, find_el), 'A')

    def test_union(self):
        parent = {'A': 'A', 'B': 'A', 'C': 'B', 'D': 'D'}
        rank = {'A': 2, 'B': '1', 'C': 0, 'D': 0}
        lab5.union(parent, rank, 'B', 'D')
        new_parent = {'A': 'A', 'B': 'A', 'C': 'B', 'D': 'A'}
        new_rank = {'A': 2, 'B': '1', 'C': 0, 'D': 0}
        self.assertEqual(parent, new_parent)
        self.assertEqual(rank, new_rank)

    def test_find_min_spanning_tree(self):
        vertices = ['A', 'B', 'C', 'D', 'E', 'F']
        edges = [Edge('A', 'B', 2),
                 Edge('B', 'C', 5),
                 Edge('B', 'D', 2),
                 Edge('C', 'E', 2),
                 Edge('C', 'D', 4),
                 Edge('D', 'E', 3),
                 Edge('D', 'F', 7),
                 Edge('E', 'F', 8)]
        min_spanning_tree = [Edge('A', 'B', 2),
                             Edge('B', 'D', 2),
                             Edge('C', 'E', 2),
                             Edge('D', 'E', 3),
                             Edge('D', 'F', 7)]
        self.assertEqual(lab5.find_min_spanning_tree(vertices, edges),
                         min_spanning_tree)


if __name__ == '__main__':
    unittest.main()
