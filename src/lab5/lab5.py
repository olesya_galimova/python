from dataclasses import dataclass


@dataclass()
class Edge:
    first: str
    second: str
    length: int


def make_set(parent, rank, i):
    parent[i] = i
    rank[i] = 0


def find(parent, i):
    if i != parent[i]:
        parent[i] = find(parent, parent[i])
    return parent[i]


def union(parent, rank, i, j):
    id_i = find(parent, i)
    id_j = find(parent, j)
    if id_i == id_j:
        return
    if rank[id_i] > rank[id_j]:
        parent[id_j] = id_i
    else:
        parent[id_i] = id_j
        if rank[id_i] == rank[id_j]:
            rank[id_j] += 1


def find_min_spanning_tree(vertex, edges):
    parent = {}
    rank = {}
    spanning_tree = []
    for i in vertex:
        make_set(parent, rank, i)
    edges.sort(key=lambda item: item.length)
    for edge in edges:
        if len(spanning_tree) + 1 == len(vertex):
            break
        if find(parent, edge.first) == find(parent, edge.second):
            continue
        union(parent, rank, edge.first, edge.second)
        spanning_tree.append(edge)
    return spanning_tree


# vertices = ['A', 'B', 'C', 'D', 'E', 'F']
# edges = [Edge('A', 'B', 2),
#          Edge('B', 'C', 5),
#          Edge('B', 'D', 2),
#          Edge('C', 'E', 2),
#          Edge('C', 'D', 4),
#          Edge('D', 'E', 3),
#          Edge('D', 'F', 7),
#          Edge('E', 'F', 8)]
# print(find_min_spanning_tree(vertices, edges))
# vertices = [j for j in input().split()]
# s = input()
# edges = []
# while s:
# s.split()
# edges.append(Edge(s[0], s[1], int(s[3])))
# s = input()
# find_min_spanning_tree(vertices, edges)
