import unittest
import lab6
from lab6 import Priority
from lab6 import Queue


class TestStringMethods(unittest.TestCase):

    def test_make_queue(self):
        graph = {'A': ['B', 'C', 'D'],
                 'B': ['A', 'C', 'D'],
                 'C': ['A', 'B', 'D', 'E', 'F'],
                 'D': ['A', 'B', 'C', 'F'],
                 'E': ['C', 'F'],
                 'F': ['C', 'D', 'E']}
        cost = {'A': 5,
                'B': 7,
                'C': 2,
                'D': 3,
                'E': 8,
                'F': 6}
        Q = Queue(graph, cost)
        queue = [Priority(2, 'C'),
                 Priority(3, 'D'),
                 Priority(5, 'A'),
                 Priority(7, 'B'),
                 Priority(8, 'E'),
                 Priority(6, 'F')]
        keys = {'A': 2,
                'B': 3,
                'C': 0,
                'D': 1,
                'E': 4,
                'F': 5}
        self.assertEqual(Q.queue, queue)
        self.assertEqual(Q.keys, keys)

    def test_get_min(self):
        graph = {'A': ['B', 'C', 'D'],
                 'B': ['A', 'C', 'D'],
                 'C': ['A', 'B', 'D', 'E', 'F'],
                 'D': ['A', 'B', 'C', 'F'],
                 'E': ['C', 'F'],
                 'F': ['C', 'D', 'E']}
        cost = {'A': 5,
                'B': 7,
                'C': 2,
                'D': 3,
                'E': 8,
                'F': 6}
        Q = Queue(graph, cost)
        self.assertEqual(Q.get_min(), 'C')

    def test_extruct_min(self):
        graph = {'A': ['B', 'C', 'D'],
                 'B': ['A', 'C', 'D'],
                 'C': ['A', 'B', 'D', 'E', 'F'],
                 'D': ['A', 'B', 'C', 'F'],
                 'E': ['C', 'F'],
                 'F': ['C', 'D', 'E']}
        cost = {'A': 5,
                'B': 7,
                'C': 2,
                'D': 3,
                'E': 8,
                'F': 6}
        Q = Queue(graph, cost)
        queue = [Priority(3, 'D'),
                 Priority(6, 'F'),
                 Priority(5, 'A'),
                 Priority(7, 'B'),
                 Priority(8, 'E')]
        keys = {'A': 2,
                'B': 3,
                'D': 0,
                'E': 4,
                'F': 1}
        self.assertEqual(Q.extract_min(), 'C')
        self.assertEqual(Q.keys, keys)
        self.assertEqual(Q.queue, queue)

    def test_change_prior(self):
        graph = {'A': ['B', 'C', 'D'],
                 'B': ['A', 'C', 'D'],
                 'C': ['A', 'B', 'D', 'E', 'F'],
                 'D': ['A', 'B', 'C', 'F'],
                 'E': ['C', 'F'],
                 'F': ['C', 'D', 'E']}
        cost = {'A': 5,
                'B': 7,
                'C': 2,
                'D': 3,
                'E': 8,
                'F': 6}
        Q = Queue(graph, cost)
        Q.change_prior('D', 1)
        queue = [Priority(1, 'D'),
                 Priority(2, 'C'),
                 Priority(5, 'A'),
                 Priority(7, 'B'),
                 Priority(8, 'E'),
                 Priority(6, 'F')]
        keys = {'A': 2,
                'B': 3,
                'C': 1,
                'D': 0,
                'E': 4,
                'F': 5}
        self.assertEqual(Q.queue, queue)
        self.assertEqual(Q.keys, keys)

    def test_Prior(self):
        graph = {'A': ['B', 'C', 'D'],
                 'B': ['A', 'C', 'D'],
                 'C': ['A', 'B', 'D', 'E', 'F'],
                 'D': ['A', 'B', 'C', 'F'],
                 'E': ['C', 'F'],
                 'F': ['C', 'D', 'E']}
        matrix = {('A', 'B'): 5,
                  ('A', 'C'): 6,
                  ('A', 'D'): 4,
                  ('B', 'A'): 5,
                  ('B', 'C'): 1,
                  ('B', 'D'): 2,
                  ('C', 'A'): 6,
                  ('C', 'B'): 1,
                  ('C', 'D'): 2,
                  ('C', 'E'): 5,
                  ('C', 'F'): 3,
                  ('D', 'A'): 4,
                  ('D', 'B'): 2,
                  ('D', 'C'): 2,
                  ('D', 'F'): 4,
                  ('E', 'C'): 5,
                  ('E', 'F'): 4,
                  ('F', 'C'): 3,
                  ('F', 'D'): 4,
                  ('F', 'E'): 4}
        tree = {'A': None,
                'B': 'D',
                'C': 'B',
                'D': 'A',
                'E': 'F',
                'F': 'C'}
        self.assertEqual(lab6.Prim(graph, matrix), tree)


if __name__ == '__main__':
    unittest.main()
