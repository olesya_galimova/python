from dataclasses import dataclass


@dataclass()
class Priority:
    prior: int
    key: str


class Queue:
    queue: list
    size: int
    keys: dict

    def __init__(self, graph, cost):
        self.size = 0
        self.queue = []
        self.keys = {}
        i = 0
        for vert in graph:
            self.__insert(Priority(cost[vert], vert))
            i += 1

    def get_min(self):
        return self.queue[0].key

    def __get_parent(self, i):
        if i % 2 == 0:
            return int(i/2 - 1)
        else:
            return int((i + 1)/2 - 1)

    def __get_left_child(self, i):
        return i*2 + 1

    def __get_right_child(self, i):
        return i*2 + 2

    def __swap(self, i, j):
        buf = self.queue[i]
        self.keys[self.queue[j].key] = i
        self.keys[self.queue[i].key] = j
        self.queue[i] = self.queue[j]
        self.queue[j] = buf

    def __shift_up(self, i):
        parent = self.__get_parent(i)
        while i > 0 and self.queue[parent].prior > self.queue[i].prior:
            self.__swap(i, parent)
            i = parent
            parent = self.__get_parent(i)

    def __shift_down(self, i):
        while True:
            max_ind = i
            l_son = self.__get_left_child(i)
            r_son = self.__get_right_child(i)
            if l_son < self.size and\
                    self.queue[max_ind].prior > self.queue[l_son].prior:
                max_ind = l_son
            if r_son < self.size and\
                    self.queue[max_ind].prior > self.queue[r_son].prior:
                max_ind = r_son
            if i != max_ind:
                self.__swap(i, max_ind)
                i = max_ind
            else:
                break

    def __insert(self, new_el):
        self.queue.append(new_el)
        self.keys[new_el.key] = self.size
        self.__shift_up(self.size)
        self.size += 1

    def extract_min(self):
        min_el = self.queue[0]
        self.queue[0] = self.queue[self.size - 1]
        self.keys.pop(min_el.key)
        self.size -= 1
        self.keys[self.queue[self.size].key] = 0
        self.queue.pop(self.size)
        self.__shift_down(0)
        return min_el.key

    def change_prior(self, el, prior):
        ind = self.keys[el]
        old_prior = self.queue[ind].prior
        self.queue[ind].prior = prior
        if prior < old_prior:
            self.__shift_up(ind)
        else:
            self.__shift_down(ind)

    def __contains__(self, item):
        if item in self.keys:
            return True
        return False

    def __bool__(self):
        return self.size > 0


def Prim(graph, len_edges):
    cost = {}
    tree = {}
    for vert in graph:
        cost[vert] = float("inf")
        tree[vert] = None
    for vert in graph:
        cost[vert] = 0
        break
    queue = Queue(graph, cost)
    while queue:
        vert = queue.extract_min()
        for i in graph[vert]:
            if i in queue and cost[i] > len_edges[(vert, i)]:
                cost[i] = len_edges[(vert, i)]
                tree[i] = vert
                queue.change_prior(i, cost[i])
    return tree


#  graph = {'A': ['B', 'C', 'D'],
#         'B': ['A', 'C', 'D'],
#        'C': ['A', 'B', 'D', 'E', 'F'],
#        'D': ['A', 'B', 'C', 'F'],
#          'E': ['C', 'F'],
#         'F': ['C', 'D', 'E']}
#  matrix = {('A', 'B'): 5,
#         ('A', 'D'): 4,
#         ('B', 'A'): 5,
#          ('B', 'C'): 1,
#          ('B', 'D'): 2,
#          ('C', 'A'): 6,
#          ('C', 'B'): 1,
#          ('C', 'D'): 2,
#          ('C', 'E'): 5,
#          ('C', 'F'): 3,
#          ('D', 'A'): 4,
#          ('D', 'B'): 2,
#          ('D', 'C'): 2,
#         ('D', 'F'): 4,
#          ('E', 'C'): 5,
#          ('E', 'F'): 4,
#           ('F', 'C'): 3,
#          ('F', 'D'): 4,
#          ('F', 'E'): 4}
#  print(Prim(graph, matrix))
